import time
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from firebase_admin import auth
import requests
from datetime import datetime, timedelta
import pytz
import random

from flask import Flask
from flask import request
from flask import make_response
from flask_cors import CORS, cross_origin

import load_kb

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, LocationMessage, LocationSendMessage
)

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

list_response_gejala = ["Oke ada lagi kak ?","Oh begitu kak, ada yang lainnya tidak kak?","Oke deh kak, gejala lainnya ada?","ada gejalanya yang lain gk kak?"]
# firebase
cred = credentials.Certificate("./serviceAccountKey.json")
firebase_admin.initialize_app(cred,{
    'databaseURL' : 'https://chatbot-kesehatan.firebaseio.com/'
})

app = Flask(__name__)

CORS(app)

line_bot_api = LineBotApi('FdSGuTbKPrvGJv56SrzkBCQVxATR6IR046A/FrJhUlKFhyM0L+dx6kjkRKZiyOQeeQvrqcpix1RTt8TsD2QLYu7eZLpzesJDkskU+E/0TwFPSMLyIVfV9RmkXPBEKs+ZqXxyg5CizM742LLc6bTezwdB04t89/1O/w1cDnyilFU=')
handler = WebhookHandler('78d3c6f9cd4e7b13ccac741b670dcb09')


@app.route('/call', methods=['POST'])
def call():

    signature = request.headers['X-Line-Signature']

    # mengambil text
    body = request.get_data(as_text=True)

    #handler
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        print("Error, key salah")
        abort(400)

    return "OK"

@app.route('/broadcast', methods=['POST'])
def broadcast():
    data = request.form.get('data')
    database = db.reference()
    snapshot = database.child("user").get()
    for key, val in snapshot.items():
        uid = key
        line_bot_api.push_message(uid,TextSendMessage(text=data))
    return data

@handler.add(MessageEvent)
def handle_message(event):
    try:
        date_now = datetime.strftime(datetime.now(pytz.timezone('Asia/Jakarta')),"%Y-%m-%d")
        database = db.reference()
        print(event)
            
        uid = event.source.user_id

        profile = line_bot_api.get_profile(uid)

        #add new user
        try:
            user = database.child("user").child(uid).get()
            if (user["is_diag"]=="0"):
                database.child("user").child(uid).update({
                    "last_chat": date_now
                })
        except Exception as e:
            database.child("user").child(uid).set({
                "name": profile.display_name,
                "is_diag": "0",
                "is_saran": "0",
            })

        #first chat
        try:
            user = database.child("user").child(uid).get()
            if (user["first_chat"]=="0"):
                database.child("user").child(uid).update({
                    "last_chat": date_now
                })
        except Exception as e:
            database.child("user").child(uid).update({
                "first_chat": date_now
            })

        database.child("user").child(uid).update({
            "last_chat": date_now
        })

        #if location
        if(event.message.type=="location"):
            lat = event.message.latitude
            long = event.message.longitude

            #call res api
            response = requests.get("https://discover.search.hereapi.com/v1/discover?at="+str(lat)+","+str(long)+"&q=hospital&apikey=IU3ZfvOExMogsdVRIDguJWez_Wi7yBrXzn3kHFkgg5k&limit=5")
            response = response.json()

            database.child("user").child(uid).update({
                "lat": str(lat),
                "long": str(long),
            })

            line_bot_api.push_message(
                    uid,
                    TextSendMessage(text="Ini lokasi-lokasi rumah sakit terdekat dari lokasi mu kak 😄"))
            for i in response["items"]:
                print(i)
                line_bot_api.push_message(
                    uid,
                    LocationSendMessage(
                        title=i["title"],
                        address=i["address"]["label"],
                        latitude=i["position"]["lat"],
                        longitude=i["position"]["lng"]
                    ))
            return "success"


        data = event.message.text
        #stemming
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()

        word_parsed = stemmer.stem(data)

        user = database.child("user").child(uid).get()
        if (user["is_diag"]=="1"):
            if (user["is_umur"]=="1"):
                database.child("user").child(uid).update({
                    "gejala": None,
                    "is_diag": "1",
                    "is_saran": "0",
                    "is_umur": "0",
                    "is_gender": "1",
                    "umur" : word_parsed
                })
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text="Kalau gendernya kak "+profile.display_name+" apa ya? laki-laki atau perempuan \n\n( 𝗖𝗼𝗻𝘁𝗼𝗵 𝗝𝗮𝘄𝗮𝗯𝗮𝗻 : 𝗹𝗮𝗸𝗶-𝗹𝗮𝗸𝗶/𝐩𝐞𝐫𝐞𝐦𝐩𝐮𝐚𝐧 )"))
                return "success"
            elif (user["is_gender"]=="1"):
                if (word_parsed=="laki-laki" or word_parsed=="laki" or word_parsed=="L" or word_parsed=="l" or word_parsed=="pria" or word_parsed=="man"):
                    word_parsed = "L"
                elif (word_parsed=="perempuan" or word_parsed=="wanita" or word_parsed=="p" or word_parsed=="P" or word_parsed=="woman"):
                    word_parsed = "P"
                else:
                    line_bot_api.reply_message(
                        event.reply_token,
                        TextSendMessage(text="Maak kak, sepertinya format jawabannya sebelumnya salah\n\nKalau gendernya kak "+profile.display_name+" apa ya? laki-laki atau perempuan \n\n( 𝗖𝗼𝗻𝘁𝗼𝗵 𝗝𝗮𝘄𝗮𝗯𝗮𝗻 : 𝗹𝗮𝗸𝗶-𝗹𝗮𝗸𝗶/𝐩𝐞𝐫𝐞𝐦𝐩𝐮𝐚𝐧 )"))
                    return "success"
                    
                database.child("user").child(uid).update({
                    "gejala": None,
                    "is_diag": "1",
                    "is_saran": "0",
                    "is_umur": "0",
                    "is_gender": "0",
                    "gender": word_parsed
                })
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text="Oke baik kak, Boleh kasih tau kak gejalanya?\ntulis satu-satu ya kak \n\n( 𝗖𝗼𝗻𝘁𝗼𝗵 𝗝𝗮𝘄𝗮𝗯𝗮𝗻 : 𝘀𝗮𝘆𝗮 𝗺𝗲𝗿𝗮𝘀𝗮 𝗱𝗲𝗺𝗮𝗺 )"))
                return "success"
            #start diagnosis
            elif (data.lower()=="sudah" or data.lower()=="sdh"):
                list_fact = []
                snapshot = database.child("user").child(uid).child("gejala").order_by_key().get()
                for key, val in snapshot.items():
                    list_fact.append(val["fact"])

                gender = database.child("user").child(uid).child("gender").get()

                penyakit,return_value,is_success = load_kb.calculate(list_fact,gender,profile.display_name)
                #return result
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text=return_value))

                #clear db
                database.child("user").child(uid).update({
                    "gejala": None,
                    "is_diag": "0",
                    "is_saran": "0",
                })
                

                if (is_success):
                    #save result
                    database.child("result").push({
                        "data": penyakit,
                        "uid": uid,
                        "name": profile.display_name,
                        "date" : date_now
                    })

            else:
                validate = load_kb.check_is_exist(list_fact)
                if (validate):
                    database.child("user").child(uid).child("gejala").push({
                        "fact": word_parsed
                    })
                    database.child("user").child(uid).update({
                        "last_fact": word_parsed
                    })
                    #return result
                    line_bot_api.reply_message(
                        event.reply_token,
                        TextSendMessage(text=random.choice(list_response_gejala)+"\nkalau sudah tidak ada boleh balas pesan ini dengan 𝘀𝘂𝗱𝗮𝗵"))
                else:
                    line_bot_api.reply_message(
                        event.reply_token,
                        TextSendMessage(text="Maaf kak, sepertinya gejala yang kaka masukan tidak ditemukan, silahkan masukan gejala yang lainnya kak"))
        elif(user["is_saran"]=="1"):
            database.child("user").child(uid).update({
                "gejala": None,
                "is_diag": "0",
                "is_saran": "0",
            })

            #save result
            database.child("feedback").push({
                "data": data,
                "uid": uid,
                "name": profile.display_name,
                "date" : date_now
            })
            
            #return result
            line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Terima kasih kak "+profile.display_name+", sarannya sudah kami terima"))
        else:
            #if start diagnosis
            if (data=="!diagnosis"):
                #start Diagnosis
                database.child("user").child(uid).update({
                    "gejala": None,
                    "is_diag": "1",
                    "is_saran": "0",
                    "is_umur": "1",
                    "is_gender": "0"
                })
                #return result
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text="Oke kak , kita mulai ya\nTapi sebelumnya mau nanya dulu nih kak, kakak umurnya berapa ya? ( 𝗖𝗼𝗻𝘁𝗼𝗵 𝗝𝗮𝘄𝗮𝗯𝗮𝗻 : 𝟭𝟵 𝘁𝗮𝗵𝘂𝗻 )"))

            #if start saran
            elif (data=="!saran"):
                #start saran
                database.child("user").child(uid).update({
                    "gejala": None,
                    "is_diag": "0",
                    "is_saran": "1",
                })
                #return result
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text="Silahkan tulis masukan dan sarannya ya kak"))
            #if start saran
            elif (data=="!tentang"):
                about = database.child("about").get()
                
                #return result
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text=about))
            else:
                #start NLP
                #return result
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text="Maaf kak, Saya belum mengerti perkataan kamu nih. tapi kalau kamu bertanya tentang fitur Diagnosis, coba balas dengan !diagnosis"))
        
        #add to chat
        database.child("chat").push({
            "uid" : uid,
            "message" : data,
            "date" : date_now
        })
        
    except Exception as e:
        print(e)
            
    
if __name__ == '__main__':
    port = int(os.getenv('PORT', 8080))

    print ("Starting app on port %d" %(port))

    app.run(debug=False, port=port, host='0.0.0.0')
