#Tujuan : mendiagnosis penyakit dari fast yang diberikan
#Input : fact
#Proses : forward chaining
  
import json 

#
#input : string,list
#output : bool
def check_exist_in_list(input_data,rules):
    r=False
    for fact in rules:
        if fact in input_data:
            r=True
            break
    return r


def check_is_exist(inputs_data):
    # Opening JSON file 
    f = open('knowledge.json',) 
    
    # returns JSON object as  
    # a dictionary 
    data = json.load(f) 


    ######
    ###### ENGINE START
    ######

    list_penyakit = []
    list_diag = []
    list_diag = json.loads(json.dumps(list_diag))

    # input_data dari array
    for input_data in inputs_data:
        # membaca data dari Knowledge Base
        for fact in data['target']:
            #checking gender
            if (fact['gender'] == gender or fact['gender'] == ""):
                # membaca apakah list contain  
                if check_exist_in_list(input_data,fact['rules']):
                    if fact['name'] in list_penyakit:
                        index = list_penyakit.index(fact['name'])
                        list_diag[index]["p"] = list_diag[index]["p"]+1
                    else:
                        list_diag.append({
                            "name" : fact['name'],
                            "ket" : fact['ket'],
                            "p" : 1,
                            "length_rules" : len(fact['rules']),
                            "rules" : fact['rules']
                        })
                        list_penyakit.append(fact['name'])
    # Closing file 
    f.close()

    if (len(list_penyakit)>0):
        return 1
    else:
        return 0
    
def calculate(inputs_data,gender,name):
    # Opening JSON file 
    f = open('knowledge.json',) 
    
    # returns JSON object as  
    # a dictionary 
    data = json.load(f) 


    ######
    ###### ENGINE START
    ######

    list_penyakit = []
    list_diag = []
    list_diag = json.loads(json.dumps(list_diag))

    # input_data dari array
    for input_data in inputs_data:
        # membaca data dari Knowledge Base
        for fact in data['target']:
            #checking gender
            if (fact['gender'] == gender or fact['gender'] == ""):
                # membaca apakah list contain  
                if check_exist_in_list(input_data,fact['rules']):
                    if fact['name'] in list_penyakit:
                        index = list_penyakit.index(fact['name'])
                        list_diag[index]["p"] = list_diag[index]["p"]+1
                    else:
                        list_diag.append({
                            "name" : fact['name'],
                            "ket" : fact['ket'],
                            "p" : 1,
                            "length_rules" : len(fact['rules']),
                            "rules" : fact['rules']
                        })
                        list_penyakit.append(fact['name'])
    # Closing file 
    f.close()

    #apply rules
    result=""
    max_fixed=0
    max_presentase=0
    max_pre=0
    max_ket=""
    for diag in list_diag:
        #calculate prob
        #checking tules chanining
        add_p=0
        for r in diag['rules']:
            # membaca apakah list contain  
            if r in list_penyakit:
                #multiple rules
                index = list_penyakit.index(r)
                diag["p"] = diag["p"] + 1
                add_p = list_diag[index]["p"] -2
                diag["length_rules"] = list_diag[index]["length_rules"] + diag["length_rules"]
        diag["presentase"] = round((diag["p"]+add_p)/diag["length_rules"],2)
        diag["fixed"] = round((diag["p"]+add_p)/len(inputs_data),2)
        max_pre = max_pre + diag["presentase"]

        #get conclution
        if not any(diag["name"] in s for s in inputs_data):
            if (diag["fixed"]>max_fixed):
                result=diag
                max_fixed=diag["fixed"]
                max_presentase=diag["presentase"]
                max_name = diag["name"]
                max_ket = diag["ket"]
            elif(diag["fixed"]==max_fixed):
                if (diag["presentase"]>max_presentase):
                    result=diag
                    max_fixed=diag["fixed"]
                    max_presentase=diag["presentase"]
                    max_name = diag["name"]
                    max_ket = diag["ket"]
                    
    if (result!="" and len(list_diag)>0):
        #new data
        n=0
        return_value = f"""Kak {name}, kemungkinan penyakitnya adalah\n\n"""
        for diag in list_diag:
            if (diag["presentase"]>0.15 and diag["p"]/diag["length_rules"]>0.2):
                return_value = return_value + f"""{diag["name"]} \nAkurasi ({round((diag["p"]/diag["length_rules"])*100,2)} %)\n\n"""
                n=n+1
        x=0
        for diag in list_diag:
            if (x==0):
                return_value = return_value + f"""\n{max_ket}"""
                x=x+1
            if (diag["presentase"]>0.15 and diag["p"]/diag["length_rules"]>0.2):
                another_pain = ""
                for r in diag['rules']: 
                    if not any(r in s for s in inputs_data):
                        another_pain = another_pain + r + "\n"

                if (another_pain!=""):
                    return_value = return_value + f"""\n\n𝗢𝗵 𝗶𝘆𝗮, 𝗮𝗱𝗮 𝗴𝗲𝗷𝗮𝗹𝗮 𝗹𝗮𝗶𝗻𝗻𝘆𝗮 𝗱𝗮𝗿𝗶 {diag["name"]} :\n\n"""+another_pain

        if (n==0):
            return_value="Maaf kak, kami belum dapat menemukan penyakit terkait gejala tersebut :("
            return None,return_value,False
        '''
        penyakit_name = result["name"]
        if (result["presentase"]>0.3):
            #parse conclution
            return_value = f"""Kak {name}, kemungkinan penyakitnya adalah {penyakit_name}\n\n"""  

            return_value = return_value + f"""{result["ket"]}\n\n"""  

            another_pain = ""
            for r in result['rules']: 
                if not any(r in s for s in inputs_data):
                    another_pain = another_pain + r + "\n"

            if (another_pain!=""):
                return_value = return_value + f"""Oh iya, ada 𝗴𝗲𝗷𝗮𝗹𝗮 𝗹𝗮𝗶𝗻𝗻𝘆𝗮 dari {penyakit_name} seperti :\n\n"""+another_pain
        elif(result["presentase"]>0.2):
            #parse conclution
            return_value = f"""Maaf Kak {name}, 𝗮𝗸𝘂 𝗯𝗲𝗹𝘂𝗺 𝗱𝗮𝗽𝗮𝘁 𝗽𝗲𝗻𝘆𝗮𝗸𝗶𝘁𝗻𝘆𝗮 𝗻𝗶𝗵, tapi bisa jadi penyakitnya adalah {penyakit_name}\n\n"""  

            return_value = return_value + f"""{result["ket"]}\n\n"""  

            another_pain = ""
            for r in result['rules']: 
                print(r)
                if not any(r in s for s in inputs_data):
                    another_pain = another_pain + r + "\n"

            if (another_pain!=""):
                return_value = return_value + f"""Oh iya, ada gejala lainnya dari {penyakit_name} seperti :\n\n"""+another_pain
        else:
            return_value="Maaf kak, kami belum dapat menemukan penyakit terkait gejala tersebut :("
    '''
    else:
        return_value="Maaf kak, kami belum dapat menemukan penyakit terkait gejala tersebut :("
        return None,return_value,False

    return_value = return_value + "\n\nUntuk langkah selanjutnya tolong share lokasinya dong kak, supaya bisa mendapatkan lokasi rumah sakit terdekat. 👍👍👍"
    return max_name,return_value,True

    ######
    ###### ENGINE STOP
    ###### 

### data test
"""
print(calculate(["saya lagi demam","terus mual mual","nyeri otot"],"L","kevin")) #Diare
print(calculate(["kencing nanah","sulit buang air kecil"],"L","kevin")) #Gonore
print(calculate(["vagina bengkak","sulit berjalan","keluar nanah"],"P","kevin")) #Kista Bartholin
print(calculate(["kram perut","berdarah","diare"],"P","kevin")) #Endometriosis
print(calculate(["demam","sakit kepala","muntah"],"L","kevin")) #Malaria
print(calculate(["muntah","kencing nanah","diare"],"L","kevin")) #Gonore
print(calculate(["diare","vagina bengkak","kram perut"],"P","kevin")) #Endometriosis
print(calculate(["diare","sesak nafas","sulit tidur","suka ngantuk","batuk batuk"],"L","kevin")) #Asma
print(calculate(["diare","vagina bengkak","sering pingsan"],"P","kevin")) #Asma
print(calculate(["muntah","kencing nanah","diare"],"L","kevin")) #Gonore
print(calculate(["sakit tenggorokan","pilek","kram perut"],"P","kevin")) #Difteri
print(calculate(["mengigil","sesak nafas","sulit tidur","mengigil"],"L","kevin")) #Asma
print(calculate(["diare","vagina bengkak","kecing nanah"],"P","kevin")) #Kista Bartholin
print(calculate(["batuk-batuk","pusing kepala","sering pingsan"],"L","kevin")) #Asma
print(calculate(["vagina bengkak","kencing nanah","diare"],"P","kevin")) #Kista Bartholin
print(calculate(["lemas","pilek","berkeringat","diare"],"P","kevin")) #Malaria
print(calculate(["mengigil","mata memerah","diare","demam"],"L","kevin")) #Campak
print(calculate(["nyeri otot","turun berat badan","demam"],"P","kevin")) #Tifus
print(calculate(["nyeri otot","sesak nafas","muntah muntah","mual","batuk batuk"],"L","kevin")) #Demam Berdarah
print(calculate(["diare","vagina bengkak","sering pingsan"],"P","kevin")) #Asma
print(calculate(["muntah","kencing nanah","diare"],"L","kevin")) #Gonore
print(calculate(["sakit tenggorokan","nyeri otot","batuk kering","diare"],"P","kevin")) #Tifus
print(calculate(["mengigil","sesak nafas","sulit tidur","mengigil"],"L","kevin")) #Asma
print(calculate(["diare","vagina bengkak","kecing nanah"],"P","kevin")) #Kista Bartholin
print(calculate(["muncul benjolan di leher","pusing kepala","lemas","mengigil"],"L","kevin")) #Difteri
print(calculate(["vagina membengkak","kencing nanah","diare"],"P","kevin")) #Kista Bartholin
print(calculate(["lemas","pilek","berkeringat","diare"],"P","kevin")) #Malaria
print(calculate(["hidung tersumbat","hidung tersumbat","flu"],"L","kevin")) #Influenza
print(calculate(["sakit punggung","diare","sakit saat buang air besar"],"P","kevin")) #Endometriosis

print(calculate(["benjol kecil","vagina bengkak","demam"],"P","kevin")) #Kista Bartholin


print(calculate(["benjol kecil","vagina bengkak","demam"],"P","kevin")) #Kista Bartholin
print(calculate(["sering kelelahan","nyeri otot terkadang","batuk kering","muntah"],"P","kevin")) #Tifus
"""